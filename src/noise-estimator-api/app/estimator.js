// Copyright (C) 2018 ATOS Spain
// 
// This file is part of Synchronicity.
// 
// Synchronicity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Synchronicity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Synchronicity.  If not, see <http://www.gnu.org/licenses/>.

const _ = require('lodash');
// const prettyHrtime = require('pretty-hrtime');
// const moment = require('moment');
const Promise = require('bluebird');

const config = require('../config/config');
const elastic = require('./elasticsearch-service').instance;
const logger = require('../utils/log')(module);


class Estimator {

  constructor() {
    // Singleton class
    if (!Estimator.instance) {
      Estimator.instance = this;
    }

    // this.history = new history.HistoricValues();
    return Estimator.instance;
  }


  /**
   * Call to estimator (parameter to pass it the ID of the context element; all the resting ones are for processing the results)
   * @param {Object} context Context entity description (as it comes from Orion CB)      
   * ToDo- Handle times (e.g. what happens when we have not received an observation in days, etc.)
   */
  estimation(context) {

    return new Promise(async function (resolve, reject) {
      
      let output = {};            
      const time_labels = ['dateObserved', 'dateModified', 'dateRetrieved', 'TimeInstant', 'time_index', 'modifiedAt'];

      logger.debug('Estimation called - ' + context.id);

      try {        
        let response = await elastic.searchLastObservation(config.elasticsearch.predictions_index, context.id);

        if (!_.isEmpty(response)) {

          output[config.api.estimator_attr] = {
            type: 'StructuredValue',
            value: _.concat(config.ai_prediction_module.data_aggregation ?
              config.ai_prediction_module.dataAggregation(context, {}, true) :
              config.ai_prediction_module.prediction_attribute,
              response.values),
            metadata: {
              estimation_times: {
                type: 'StructuredValue',
                value: _.concat( 
                  _.head(_.values(_.pick(context, time_labels))).value,
                  response.prediction_times)
              }
            }
          };

          resolve(_.merge(context, output));
        } else {

          reject({
            status_code: 404,
            message: 'No prediction data for ' + context.id
          });
        }
      } catch (error) {
        reject({
          status_code: 500
        });
        logger.error('Error on estimation output (' + context.id + ') - ' + JSON.stringify(error));
      }
    });
  }

}

const instance = new Estimator();
Object.freeze(instance);
exports.instance = instance;
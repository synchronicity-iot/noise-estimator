// Copyright (C) 2018 ATOS Spain
// 
// This file is part of Synchronicity.
// 
// Synchronicity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Synchronicity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Synchronicity.  If not, see <http://www.gnu.org/licenses/>.

'use strict';
const axios = require('axios');
const _ = require('lodash');
const Promise = require('bluebird');
const turf = require('@turf/turf');
const prettyHrtime = require('pretty-hrtime');
const moment = require('moment');

const config = require('../config/config.js');
const auth = require('../config/auth.js');
const logger = require('../utils/log.js')(module);
const elastic = require('./elasticsearch-service').instance;

/**
 * NOTE: This class only works with the historical API proposed in the scope of the Synchronicity project 
 * (based on STH Comet)
 */
class HistoricValues {

  constructor() {
    if (!HistoricValues.instance) {
      HistoricValues.instance = this;
    }
    return HistoricValues.instance;
  }

  /**   
   * Get all historical data of a particular context element and dump to Elastic
   * @param {String} context Context element ID
   * @returns {Object} CSV that contains all historical values
   */
  async dumpHistory(info) {
    logger.info('Dumping historic data... ');
    const start = process.hrtime();
    let elastic_first, elastic_last;

    for (let i = 0; i < info.length; i++) {
      // 1- Get the flag is enabled, we dump a number of observations BEFORE THE OLDEST DOCUMENT      
      try {
        elastic_first = await elastic.searchFirstObservation(config.elasticsearch.data_index, info[i].id);        
      } catch (error) {
        logger.error('Error on dumping (past) historical data to Elastic - ' + JSON.stringify(error));
      }
      let next = true,
        total = 0,
        data = [],
        last = !_.isEmpty(elastic_first) ? moment(elastic_first.timestamp).subtract(1, 'ms').toISOString() :
        moment().toISOString();

      while (next) {
        try {
          data = await this.getHistory(info[i], 'lastn=' + config.api.history.batch_size + '&timerel=before&time=' +
            last);
        } catch (error) {
          logger.error('Error on dumping (past) historical data to Elastic - ' + JSON.stringify(error));
        }

        if (!_.isEmpty(data)) {
          total += data.length;

          // Exit condition -- Data length shorter than batch size           
          next = (data.length === config.api.history.batch_size);
          last = moment(_.head(data).timestamp).subtract(1, 'ms').toISOString();

          if (!_.isEmpty(data) && (data.length > 1 && (config.api.history.batch_size > 1))) {
            logger.info('Historical data dump (' + info[i].id + ') - ' + _.head(data).timestamp + ' < t < ' + _.last(data).timestamp +
              ' [' + total + ']');
            try {
              await elastic.bulkIndex(data, config.elasticsearch.data_index, 'context_data');
            } catch (error) {
              logger.error('Error on dumping (past) historical data to Elastic - ' + JSON.stringify(error));
            }
          }
        } else {
          next = false;
        }
      }

      // 2- Values after the last observation
      try {
        elastic_last = await elastic.searchLastObservation(config.elasticsearch.data_index, info[i].id);
      } catch (error) {
        logger.error('Error on dumping (post) historical data to Elastic - ' + JSON.stringify(error));
      }

      next = true,
        total = 0,
        data = [],
        last = !_.isEmpty(elastic_last) ? moment(elastic_last.timestamp).add(1, 'ms').toISOString() :
        moment().toISOString();

      while (next) {
        data = await this.getHistory(info[i], 'lastn=' + config.api.history.batch_size + '&timerel=after&time=' +
          last);
        if (!_.isEmpty(data)) {
          total += data.length;
          // Exit condition -- Data length shorter than batch size           
          next = (data.length === config.api.history.batch_size);
          // This array comes in reverse order; that is why we have to sort it 
          last = moment(_.last(data).timestamp).add(1, 'ms').toISOString(); //To avoid duplicates

          if (!_.isEmpty(data) && (data.length) > 1 && (config.api.history.batch_size > 1)) {
            logger.info('Historical data dump (' + info[i].id + ') - ' + _.last(data).timestamp + ' < t < ' + _.head(data).timestamp +
              ' [' + total + ']');
            try {
              await elastic.bulkIndex(_.reverse(data), config.elasticsearch.data_index, 'context_data');
            } catch (error) {
              logger.error('Error on dumping (post) historical data to Elastic - ' + JSON.stringify(error));
            }
          }
        } else {
          next = false;
        }
      }
    }

    const end = process.hrtime(start);
    const latency = prettyHrtime(end, {
      precise: true,
      verbose: false
    });
    logger.info('Dumping historic data finished [' + latency + ']');
  }

  /**
   * Function that retrieves the historical data values of a particular context entity   
   * @param {Object} context Context information of a single device
   * @param {String} query_params URL query parameters (from the request message)
   */
  getHistory(context, query_params) {
    logger.info('<REQ> Get historical data - ' + context.id);

    // As there is no single option for annotating the timestamp, we will search one amongst the following list of elements
    // NOTE: Only the first element will be considered
    const time_labels = ['dateObserved', 'dateModified', 'dateRetrieved', 'TimeInstant', 'time_index', 'modifiedAt'];

    return new Promise((resolve, reject) => {
      const start = process.hrtime();
      let rz;

      // For this particular case, only one attribute is selected (first check)
      if (!config.api.history.attrs.length) {
        logger.error('No attributes in config-file - please check');
        throw new Error('Config file error');
      }

      // First option - to check whether the id contains the name of the RZ; in this case.
      // NOTE:  This solution relies on the ID system defined in Organicity and Synchronicity projects, hence this may 
      // be prone to modifications upon new infrastructures
      // Example: urn:ngsi-ld:TrafficFlowObserved:santander:traffic:flow:1001
      // Meaning: urn(fixed):ngsi-ld(fixed):Context entity:RZ:Service:Group:EntityName   

      const hit = _.find(_.map(config.rzs.brokers, o => {
        return _.lowerCase(o.rz);
      }), (o) => _.includes(context.id, o));

      if (hit) {
        rz = _.filter(config.rzs.brokers, {
          rz: _.capitalize(hit)
        });
      } else {
        // Workaround - As the ID does not provide any hint (in most of the RZs) of which RZ they 
        // belong to, we locate them from the context entity' location     
        let location;
        try {
          switch (context.location.value.type) {
            case 'Point':
              location = turf.point(context.location.value.coordinates);
              break;
            case 'LineString':
              location = turf.lineString(context.location.value.coordinates);
              break;
            default:
              logger.error('Type not found');
              break;
          }
        } catch (error) {
          logger.error('Cannot parse element location - ' + JSON.stringify(error));
          reject(error);
        }
        rz = _.filter(config.rzs.brokers, o => {
          return turf.booleanWithin(location, turf.bboxPolygon(_.flatten(o.area)));
        });
      }
      // End of workaround     

      if (rz && rz.length == 1) {
        axios.all(_.map(config.api.history.attrs, o => this.sendQuery(rz, context, o,
            query_params)))
          .then(
            (response) => {
              let aux = [];
              let output = [];

              // Iterative retrieval of historical values (each attribute must be requested one by one)
              _.forEach(config.api.history.attrs, attribute => {
                let index = _.indexOf(config.api.history.attrs, attribute);
                if (index >= 0 && !_.isEmpty(response[index]['data'][attribute])) {
                  _.merge(aux, _.map(response[index]['data'][attribute], item => {
                    let element = {};
                    if (!_.has(element, 'id')) {
                      element['id'] = response[index].data.id;
                    }
                    if (!_.has(element, 'timestamp')) {
                      // element['timestamp'] = _.has(item, 'time_index') ? item.time_index : item.modifiedAt;
                      const time = _.pick(item, time_labels);
                      element['timestamp'] = !_.isEmpty(time) ? _.head(_.values(time)) : undefined;
                    }
                    if (!_.has(element, 'type')) {
                      element['type'] = response[index].data.type;
                    }
                    element[attribute] = item.value;
                    return element;
                  }));
                }
              });

              // Aggregation/transformation of data (Users must tailor their own functions at config.js file)     
              try {
                if (config.ai_prediction_module.data_aggregation) {
                  logger.info('Aggregating/transforming datasets ... ');
                  output = config.ai_prediction_module.dataAggregation(context, aux);
                } else {
                  output = _.map(aux, observation => {
                    let temp = {};
                    _.forEach(config.ai_prediction_module.training_attributes, o => {
                      if (!_.has())
                        temp[o] = _.has(context, o) ? context[o].value : undefined;
                    });
                    return _.merge(observation, _.pickBy(temp, _.identity));
                  });
                }
              } catch (error) {
                logger.error('Error with the data aggregation/transformation, please check your function at config.js: ' + error);
              }

              const end = process.hrtime(start);
              const latency = prettyHrtime(end, {
                precise: true,
                verbose: false
              });

              // If the query retrieves the last N values, the output must be reversed               
              if (_.includes(query_params, 'last')) {
                logger.info('Retrieved ' + output.length + ' observations [' + latency + '] => ' + context.id);
                // ToDo - update to this feature
                logger.perf('<RESP> GET Historical data ' +
                  ' [' + latency + ']', {
                    type: 'Get Historical data',
                    latency: latency,
                  });
                resolve(_.reverse(output));
              } else {
                resolve(output);
              }
            },
            (error) => {
              logger.error('Invalid response from historic API - ' + error);
              reject(error);
            }
          );
      } else {
        logger.error('Error on locating device (' + context.id + ') in any of the RZs');
      }
    });
  }

  /**
   * Save historical data onto Elastic (this method is called via /newdata endpoint)
   * @param {Object} data JSON array with the historical data
   */
  manualDump(data) {
    return new Promise((resolve, reject) => {

      elastic.bulkIndex(data, config.elasticsearch.data_index, 'context_data').then(
        (response) => {
          logger.debug('Successfully saved data on Elasticsearch');
          response;
          resolve();
        },
        (error) => {     
          logger.error('Something happened when saving historical data - ' + error);     
          reject();
        });
    });
  }

  /**
   * 
   * @param {Object} host_info  Information about the host (extracted from file config.js)
   * @param {String} context  Context information retrieved from the CB
   * @param {String} attribute Specific attribute to query
   * @param {String} query_params 
   * @returns {Object} Query response 
   */
  sendQuery(host_info, context, attribute, query_params) {

    const endpoint = host_info[0].historic_host +
      '/v2/entities/' + context.id + '/attrs/' + attribute + '?' + query_params +
      '&type=' + context.type;

    const headers = {
      'Accept': 'application/json',
    };

    if (host_info[0].auth) {
      headers[auth.orion[host_info[0].rz].key] = auth.orion[host_info[0].rz].token;
    }

    return axios({
      method: 'get',
      url: endpoint,
      headers: headers
    });
  }
}

// exports.HistoricValues = HistoricValues;

const instance = new HistoricValues();
Object.freeze(instance);
exports.instance = instance;
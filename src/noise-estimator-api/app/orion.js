// Copyright (C) 2018 ATOS Spain
// 
// This file is part of Synchronicity.
// 
// Synchronicity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Synchronicity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Synchronicity.  If not, see <http://www.gnu.org/licenses/>.

'use strict';
const axios = require('axios');
const _ = require('lodash');
const Promise = require('bluebird');
const prettyHrtime = require('pretty-hrtime');

const logger = require('../utils/log')(module);
const config = require('../config/config');
const auth = require('../config/auth');
const subscriptions = require('./orion-subscriptions').instance;
const history = require('./historic-values').instance;
const elastic = require('./elasticsearch-service').instance;
const elastic_data = require('../utils/elastic-data');

const ORION_ENDPOINT = process.env.ORION_HOST ? process.env.ORION_HOST :
  config.orion.endpoint;

class Orion {
  constructor() {
    // Singleton class    
    if (!Orion.instance) {
      Orion.instance = this;
    }
    return Orion.instance;
  }

  /**
   * Get the information from the underlying RZs and prepare the AS Orion and Elasticsearch persistence means
   */
  async start() {
    const self = this;
    logger.info('INITIALIZING ... ');


    // 1-  Elastic search data storage (and index creation)
    try {
      await elastic.setIndex(config.elasticsearch.entities_index, elastic_data.entity_mapping);      
    } catch (error) {
      logger.error(JSON.stringify(error));
    }

    // 2- Create index for data (historic of observations)
    try {
      await elastic.setIndex(config.elasticsearch.data_index, elastic_data.data_mapping);
    } catch (error) {
      logger.error(JSON.stringify(error));
    }

    const start = process.hrtime();
    // 3- Get the information from all RZs (defined in config/config.js)
    _.forEach(config.rzs.brokers, item => {
      _.forEach(config.api.entity_types, type => {
        self.getEntitiesFromRzs(item, type).then(
          (response) => {

            // Update context info
            if (_.has(response, 'data') && response.data.length) {

              // Update Orion CB            
              self.createEntities(response.data);

              const end = process.hrtime(start);
              const latency = prettyHrtime(end, {
                precise: true,
                verbose: false
              });
              logger.debug(item.rz + '  (' + type + ') - ' +
                String(response.data.length) + ' entities [' + latency + ']');
              // Subscriptions handling              
              if (response.data.length && config.api.subscriptions.subscribe_context) {
                logger.info('Subscribe to context - ' + item.rz);
                subscriptions.start(response.config.url.substring(0, response.config.url.lastIndexOf('/')), type);
              }
            }
          },
          (error) => {
            const end = process.hrtime(start);
            const latency = prettyHrtime(end, {
              precise: true,
              verbose: false
            });
            logger.error(item.rz + '  (' + type + ') - Orion GET entities error - ' + error + ' [' + latency + ']');
          }
        );
      });
    });
  }

  /**
   * Copy the context elements from the RZs to the atomic service Orion
   * NOTE: Due to the 1MB length limitation of a request, the batch registration may not be 
   * the best option
   * ToDo (potential issue) -  If the problem raises, we will have to deal with the fragmentation of the object, 
   * split into various requests
   * (source): https://fiware-orion.readthedocs.io/en/0.27.0/user/known_limitations/
   * @param{Object} context Context information (array format)
   */
  async createEntities(context) {    

    // 1- Atomic service Orion CB (to be deprecated)
    const headers = {
      'Content-Type': 'application/json',
      'Fiware-service': _.has(config.orion, 'fiware_service') ? config.orion.fiware_service : '',
      'Fiware-servicepath': _.has(config.orion, 'fiware_servicepath') ? config.orion.fiware_servicepath : ''
    };
    const body = {
      actionType: 'APPEND',
      entities: context
    };

    try {
      await axios({
        method: 'post',
        url: ORION_ENDPOINT + '/v2/op/update',
        headers: headers,
        data: body
      });
      logger.debug('[ORION] added/updated (' + context.length + ' entities)');

    } catch (error) {
      logger.error('Cannot add/update context elements to Orion - ' + error);
    }

    // 2 - Bulk entities creation/update (Elastic)
    try {
      await elastic.bulkIndex(context, config.elasticsearch.entities_index, 'context_entities');      
      logger.info('[Elastic] Added/updated (' + context.length + ' entities)');
    } catch (error) {
      logger.error('Cannot send bulk context info - ' + error);
    }

    // 3- Historical data dump (if enabled)
    if (config.api.history.dump_history) {
      history.dumpHistory(context);
    }
    else {
      logger.info ('Historical data dump disabled at config.js file');
    }
  }

  /**
   * Get all entities of type X
   * NOTE: This current version only supports up to 1000 context elements
   * @param {Object} host_info Information about the host (extracted from file config.js)
   * @param {String} type Entity type
   * @returns Array with all the context entities that belong to the specified type
   */
  getEntitiesFromRzs(host_info, type) {
    const endpoint = host_info.orion_host +
      '/v2/entities?limit=1000&type=' + type;


    const headers = {
      'Accept': 'application/json',
      'fiware-service': _.has(host_info, 'fiware_service') ? host_info['fiware_service'] : '',
      'fiware-servicepath': _.has(host_info, 'fiware_servicepath') ? host_info['fiware_servicepath'] : ''
    };

    if (host_info.auth) {
      headers[auth.orion[host_info.rz].key] = auth.orion[host_info.rz].token;
    }

    return axios({
      method: 'get',
      url: endpoint,
      headers: headers
    });
  }

  /**ztomic service's Orion
   * @param {String} Source of the search (Orion or Elastic)
   * @returns List of context entities registered in the various Context Brokers
   */
  getEntitiesAtomicService(source, id) {
    if (source === 'orion') {
      let endpoint = ORION_ENDPOINT + '/v2/entities';
      endpoint += id ? '/' + id : '?limit=1000';

      const headers = {
        'Accept': 'application/json',
        'Fiware-service': _.has(config.orion, 'fiware_service') ? config.orion.fiware_service : '',
        'Fiware-servicepath': _.has(config.orion, 'fiware_servicepath') ? config.orion.fiware_servicepath : ''
      };
      return axios({
        method: 'get',
        url: endpoint,
        headers: headers,
      });
    } else if (source === 'elastic') //Elastic search

      return new Promise(function (resolve, reject) {
        if (!id) { //Search all          
          elastic.searchAll(config.elasticsearch.entities_index).then(
            (response) => {
              resolve({
                data: response
              }); //"Trick" to keep the homogeneity with Orion
            },
            (error) => {
              logger.error('Error ' + error);
              reject(error);
            }
          );
        } else {

          elastic.searchById(config.elasticsearch.entities_index, 'context_entities', id).then(
            (response) => {
              resolve({
                data: response
              }); //"Trick" to keep the homogeneity with Orion
            },
            (error) => {              
              reject(error);
            }
          );
        }
      });
  }
}

const instance = new Orion();
Object.freeze(instance);
exports.instance = instance;
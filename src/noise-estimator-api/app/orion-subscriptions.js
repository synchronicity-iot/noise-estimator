// Copyright (C) 2019 ATOS Spain
// 
// This file is part of Synchronicity.
// 
// Synchronicity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Synchronicity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Synchronicity.  If not, see <http://www.gnu.org/licenses/>.

'use strict';
const axios = require('axios');
const _ = require('lodash');
const Promise = require('bluebird');
const elastic = require('./elasticsearch-service').instance;
const config = require('../config/config.js');
const auth = require('../config/auth.js');
const logger = require('../utils/log.js')(module);


// Aux variables
const ORION_ENDPOINT = process.env.ORION_HOST ? process.env.ORION_HOST :
  config.orion.endpoint;

class OrionSubscriptions {

  constructor() {

    // Singleton class
    if (!OrionSubscriptions.instance) {
      // Object that manages all the subscription stuff (including last observations)
      this.activeSubscriptions = new Array;

      // Flag      
      OrionSubscriptions.instance = this;
    }
    return OrionSubscriptions.instance;
  }

  /**
   * Subscribe to all entities gathered from the ORION(s) CB(s)
   * @param {String} endpoint    
   */
  start(endpoint, entity_type) {

    const body = {
      description: 'Synchronicity Estimator Atomic Service',
      subject: {
        entities: [{
          idPattern: '.*',
          type: entity_type
        }],
      },
      notification: {
        http: {
          url: process.env.SUBSCRIPTIONS_ENDPOINT ? process.env.SUBSCRIPTIONS_ENDPOINT + '/notify' : config.api.subscriptions.endpoint + '/notify'
        },
      },
      expires: '2040-01-01T14:00:00.00Z',
      throttling: 5
    };

    const hit = _.find(config.rzs.brokers, {
      orion_host: endpoint.split('/v2')[0]
    });

    let header = {
      'Content-Type': 'application/json',
      Accept: 'application/json'
    };

    if (hit) {
      // Prepare header (Auth Tokens + Fiware-service + Fiware-servicepath)
      if (hit.auth === true) {
        header[auth.orion[hit.rz].key] = auth.orion[hit.rz].token;
      }

      const temp = _.find(config.rzs.brokers, {
        rz: hit.rz
      });
      header['fiware-service'] = _.has(temp, 'fiware_service') ? temp['fiware_service'] : '';
      header['fiware-servicepath'] = _.has(temp, 'fiware_servicepath') ? temp['fiware_servicepath'] : '';
    }

    axios({
        method: 'post',
        url: endpoint + '/subscriptions',
        headers: header,
        data: body
      })
      .then(
        (response) => {
          this.activeSubscriptions.push({
            endpoint: endpoint,
            id: response.headers.location.split('/').pop(),
            started: false
          });
          logger.info('Subscription successful with ID ' + response.headers.location.split('/').pop() + ' - ' + endpoint);
        },
        (error) => {
          logger.error('Orion POST subscription error - ' + error);
        });

  }

  /**
   * Method called when a new notification message is received
   * @param {String} subscriptionId ID
   * @param {Object} data Notification's payload
   */
  async manageNotification(subscriptionId, data) {

    // As there is no single option for annotating the timestamp, we will search one amongst the following list of elements
    // NOTE: Only the first element will be considered
    const time_labels = ['dateObserved', 'dateModified', 'dateRetrieved', 'TimeInstant', 'time_index', 'modifiedAt'];

    const hit = _.find(this.activeSubscriptions, {
      'id': subscriptionId
    });

    if (hit) {
      let output = {};
      // Update context elements (ignored the first time for avoiding duplicated entries)

      if (! hit.started) {
        hit.started = true;        
      } else {

        // Await and lodash do not really get on well; hence, we have to rely on a traditional for loop
        for (let i = 0; i < data.length; i++) {

          try {
            await this.updateContext(data[i]);
          } catch (e)  {
            logger.error(e);
          }

          await this.updateElastic(data[i]).catch(e => {
            logger.error(e);
          });

          // Save data to elastic (directly or from data aggregation - config.js)
          try {
            if (!config.ai_prediction_module.data_aggregation) {
              // Default case... the output object will be composed by id, timestamp and all the attributes listed 
              // in config.api.data_aggregation.attrs     
              try {
                output = _.map(data, o => {
                  let aux = {};
                  aux['id'] = o.id;
                  aux['type'] = o.type;
                  
                  const time = _.pick(o, time_labels);
                  aux['timestamp'] = ! _.isEmpty(time) ? _.head(_.values(time)).value : undefined;

                  _.forEach(config.ai_prediction_module.training_attributes, attr => {
                    aux[attr] = _.has(o, attr) ? o[attr].value : undefined;
                  });
                  return _.pickBy(aux, _.identity);
                });
              } catch (error) {
                logger.error('Subscription data handling error ' + error);
              }
            }
            //Data aggregation function
            else {
              try {
                output = config.ai_prediction_module.dataAggregation(data);
              } catch (error) {
                logger.error('Subscription data handling error ' + error);
              }
            }

            // Save onto Elastic --> As this will be handled with single elements, we only take the object
            elastic.addToIndex(_.head(output), config.elasticsearch.data_index, 'context_data');
          } catch (error) {
            logger.error('Cannot save observation to Elastic (' + data[i].id + ') - ', error);
          }
        }
      }
    }
  }


  /**
   * Update the current values
   * @param {Object} published_data Context information received from the subscription sink
   */
  async updateContext(event) {    

    // 1- Update Orion Context Broker information
    const headers = {
      'Content-Type': 'application/json',
      'Fiware-service': _.has(config.orion, 'fiware_service') ? config.orion.fiware_service : '',
      'Fiware-servicepath': _.has(config.orion, 'fiware_servicepath') ? config.orion.fiware_servicepath : ''
    };

    // The update context method does not need any of this attributes
    const body = _.omit(event, ['id', 'type']);

    try {
      await axios({
        method: 'patch',
        url: ORION_ENDPOINT + '/v2/entities/' + event.id + '/attrs',
        headers: headers,
        data: body
      });     
      
      logger.debug('[ORION] added/updated (' + event.id + ')');

    } catch (err) {
      logger.error('Cannot add/update context elements to Orion - ' + err);
      throw new Error('Orion update error');
    }
  }

  /**
   * Update ElasticSearch entities index (do not confuse with data index)
   * @param {Object} event Notification received from Orion (context element)
   */
  async updateElastic(event) {
    return new Promise(function (resolve, reject) {
      try {
        elastic.addToIndex(event, config.elasticsearch.entities_index, 'context_entities', event.id).then(
          (response) => {
            resolve(response._id);
          },
          (error) => {
            reject(error);
          }
        );
      } catch (error) {
        logger.error('Error on updating Elastic ' + error);
      }
    });
  }

  /**
   * Close all active subscriptions
   */
  async close() {
    logger.info('---Closing Orion instance---');
    const self = this;
    return new Promise(function (resolve, reject) {
      if (self.activeSubscriptions.length) {
        axios.all(_.map(self.activeSubscriptions, o => self.unsubscribe(o.endpoint, o.id)))
          .then(
            (response) => {
              logger.info('All suscriptions cancelled');
              resolve(_.head(response).status);
            },
            (error) => {
              logger.error('Orion DELETE subscription error - ' + error);
              reject(error);
            }
          );
      } else {
        resolve();
      }
    });
  }

  /**
   * Send Unsubscribe message to ORION CB
   * @param {String} endpoint 
   * @param {String} subscriptionId 
   */
  unsubscribe(endpoint, subscriptionId) {

    let hit = _.find(config.rzs.brokers, {
      orion_host: endpoint.split('/v2')[0]
    });

    let header = {};

    if (hit) {
      header = _.pickBy({
        Accept: 'application/json',
        'fiware-service': _.has(hit, 'fiware_service') ? hit.fiware_service : undefined,
        'fiware-servicepath': _.has(hit, 'fiware_servicepath') ? hit.fiware_servicepath : undefined
      }, _.identity);
    }

    hit = _.find(config.rzs.brokers, {
      orion_host: endpoint.split('/v2')[0]
    });

    if (hit && hit.auth === true) {
      header[auth.orion[hit.rz].key] = auth.orion[hit.rz].token;
    }

    return axios({
      method: 'delete',
      url: endpoint + '/subscriptions/' + subscriptionId,
      headers: header
    });
  }
}

const instance = new OrionSubscriptions();
Object.freeze(instance);
exports.instance = instance;
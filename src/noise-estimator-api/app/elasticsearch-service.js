// Copyright (C) 2019 ATOS Spain
// 
// This file is part of Synchronicity.
// 
// Synchronicity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Synchronicity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Synchronicity.  If not, see <http://www.gnu.org/licenses/>.

const elastic = require('elasticsearch');
const _ = require('lodash');
const prettyHrtime = require('pretty-hrtime');
const {
   DataStream
} = require('scramjet');
const WritableBulk = require('elasticsearch-streams').WritableBulk;
const TransformToBulk = require('elasticsearch-streams').TransformToBulk;

const config = require('../config/config');
const logger = require('../utils/log')(module);
const Promise = require('bluebird');

const ELASTIC_MAX_QUERY_SIZE = 10000;

/**
 * Class that handles all the operations related to ElasticSearch
 */
class Elasticsearch {
   constructor() {
      if (!Elasticsearch.instance) {
         this.connect();
         Elasticsearch.instance = this;
      }
   }

   /**
    * Connect to Elasticsearch database
    */
   connect() {
      const endpoint = process.env.ELASTIC_SEARCH_HOST ? process.env.ELASTIC_SEARCH_HOST :
         config.elasticsearch.endpoint;
      try {
         this.esClient = new elastic.Client({
            host: endpoint,
            requestTimeout: Infinity
         });
         logger.debug('Successfully connected to Elasticsearch');
      } catch (error) {
         logger.error('Error on connection to Elasticsearch - ' + error);
      }
   }

   /**
    * Try to create (if it does not exist) the index with the corresponding type
    * @param {String} index Name of the index
    * @param {Object} mapping Mapping template
    */
   setIndex(index, mapping) {

      const self = this;
      return new Promise(function (resolve, reject) {

         self.esClient.indices.exists({
            index: index
         }).then(
            (response) => {
               if (response === true) {
                  logger.debug('Index ' + index + ' already exists... skipping');
                  resolve();
               } else {
                  self.esClient.indices.create({
                     index: index,
                     body: {
                        mappings: mapping
                     }
                  }).then(
                     (response) => {
                        logger.debug('Index (' + response.index + ') created successfully ');
                        resolve();
                     },
                     (error) => {
                        logger.error('Error creating the index ' + error);
                        reject();
                     }
                  );
               }
            },
            (error) => {
               reject(error);
            }
         );
      });
   }

   /**
    * Add document to index
    * @param {Object} data  Object to ne recorded
    * @param {String} index Index
    * @param {String} type Type (i.e. mapping)
    * @param {String} id (Optional) If
    */
   addToIndex(data, index, type, id = undefined) {
      const self = this;

      return new Promise(function (resolve, reject) {
         logger.info('Adding data to index - ' + index);
         self.esClient.index(_.pickBy({
            index: index,
            type: type,
            id: id ? id : undefined,
            body: data
         }, _.identity)).then(
            (response) => {
               // logger.debug('[Elastic] Document created/updated successfully ' + response._id);
               resolve(response._id);
            },
            (error) => {
               logger.error('[Elastic] Error adding element - ' + error);
               reject();
            }
         );
      });
   }

   /**
    * Insert indexes (bulk mode)
    * @param {Object} data Object(s) to insert 
    * @param {String} index Index
    * @param {String} type Type of document    
    * NOTE: The bulk API specifies a special format (named X-NDJSON) to handle bulk CRUD on Elastic 
    * (https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-bulk.html)
    */
   bulkIndex(data, index, type) {
      const self = this;
      let latency;

      // Differentiate between manual or auto-indexing (entities vs data)
      const auto_indexing = _.includes(index, 'entities') ? true : false;

      return new Promise(function (resolve, reject) {

         const start = process.hrtime();
         var bulkExec = function (bulkCmds, callback) {
            try {
               self.esClient.bulk({
                  index: index,
                  type: type,
                  body: bulkCmds
               }, callback);
            } catch (error) {
               logger.error('Error on bulkexec function ' + JSON.stringify(error));
            }
         };
         var ws = new WritableBulk(bulkExec);
         var toBulk = new TransformToBulk(function getIndexTypeId(doc) {
            return _.pickBy({
               _id: auto_indexing ? doc.id : undefined,
               _index: index,
               _type: type
            }, _.identity);
         });

         function done() {
            const end = process.hrtime(start);
            latency = prettyHrtime(end, {
               precise: true,
               verbose: false
            });
            logger.perf('<ELASTIC> Bulk injection to index "' + index + '" - (' + data.length + ')' +
               ' [' + latency + ']', {
                  type: 'Bulk injection',
                  latency: latency,
                  batch: data.length
               });
         }

         try {
            DataStream
               .from(data)
               .setOptions({
                  maxParallel: 4
               }) // set your options            
               .pipe(toBulk)
               .pipe(ws)
               .on('finish', done);
            resolve();
         } catch (error) {
            logger.error('Error on bulk injection to Elastic - ' + JSON.stringify(error));
            reject();
         }
      });
   }

   /**
    * Short query (by default < 10000 hits)
    * @param {Object} query JSON body of the query
    * @param {String} index Target index (or list of indices)
    * @param {Object} options (Optional) Complementary options (e.g. size, sort, _source_includes,  etc.) 
    * that are out of the legacy query    
    */
   async search(query, index, options = {}) {
      const self = this;
      let response = [];
      return new Promise(async function (resolve, reject) {
         try {

            const input = _.merge({
               body: query,
               index: index,
            }, options);

            response = await self.esClient.search(input);

            if ((response.hits.total > ELASTIC_MAX_QUERY_SIZE) &&
               (response.hits.hits.len === ELASTIC_MAX_QUERY_SIZE)) {
               logger.warning('Response size > Maximum allowed - Shifting to scrolled query');
               response = [];

               response = await self.scroll(query, index, options);
               resolve(response);
            } else { //Regular query
               resolve(response.hits.hits);
            }

         } catch (error) {
            logger.error('Error on regular query ' + JSON.stringify(error));
            reject(error);
         }
      });
   }


   /**
    * Provided that we expected long responses (potentially more than 10000 hits)
    * @param {Object} query JSON body of the query
    * @param {String} index Target index (or list of indices)    
    * @param {Object} options (Optional) Complementary options (e.g. size, sort, _source_includes,  etc.) 
    * that are out of the legacy query
    */
   scroll(query, index, options = {}) {
      const self = this;
      let output = [],
         scroll_id, more = true;

      const input = _.merge({
         body: query,
         index: index,
         scroll: '2m',
      }, options);

      return new Promise(async function (resolve, reject) {
         try {
            let temp = await self.esClient.search(input);

            output = temp.hits.hits;
            scroll_id = temp._scroll_id;

            while (more || (temp.length < ELASTIC_MAX_QUERY_SIZE)) {
               let temp = await self.esClient.scroll({
                  scroll_id: scroll_id,
                  scroll: '2m'
               });

               more = !_.isEmpty(temp.hits.hits);
               if (more) {
                  output = _.concat(output, temp.hits.hits);
               }
            }
            await self.esClient.clearScroll({
               body: {
                  scroll_id: scroll_id
               }
            });

            resolve(output);

         } catch (error) {
            logger.error('Error on scrolled query ' + JSON.stringify(error));
            reject(error);
         }
      });
   }


   /**
    * Get all the documents that belong to a particular index
    * @param {String} index Elastic index
    */
   searchAll(index) {
      const self = this;
      const query = {
         query: {
            match_all: {}
         },
         size: ELASTIC_MAX_QUERY_SIZE
      };
      return new Promise(function (resolve, reject) {
         self.search(query, index).then(
            (response) => {
               if (response) {
                  resolve(_.map(response, o => {
                     return o._source;
                  }));
               } else {
                  resolve([]);
               }
            },
            (error) => {
               reject(error);
            }
         );

      });
   }

   /**
    * Query for a particular entity
    * @param {String} index Elastic index
    * @param String} type Data type
    * @param {String} id Context element identifier
    */
   searchById(index, type, id) {
      const self = this;
      const query = {
         query: {
            match_phrase: {
               id: id
            }
         }
      };

      const options = {
         size: 1
      };

      return new Promise(function (resolve, reject) {
         self.search(query, index, options).then(
            (response) => {

               if (!_.isEmpty(response)) {
                  resolve(_.head(response)._source);
               } else {
                  const error = {
                     response: {
                        status: 404
                     }
                  };
                  reject(error);
               }
            },
            (error) => {
               reject(error);
            }
         );
      });
   }

   /**
    * Method that returns the last observation kept by Elastic 
    * @param {String} id Context element identifier
    */
   searchFirstObservation(index, id) {
      const self = this;
      const query = {
         query: {
            match_phrase: {
               id: id
            }
         },
      };
      const options = {
         size: 1,
         sort: 'timestamp:asc'
      };

      return new Promise(function (resolve, reject) {
         self.search(query, index, options)
            .then(
               (response) => {
                  if (!_.isEmpty(response)) {
                     resolve(_.head(response)._source);
                  } else {
                     resolve([]);
                  }
               },
               (error) => {
                  reject(error);
               }
            );
      });
   }

   /**
    * Method that returns the last observation kept by Elastic
    * @param {String} id Context element identifier
    */
   searchLastObservation(index, id) {
      const self = this;
      const query = {
         query: {
            match_phrase: {
               id: id
            }
         },
      };
      const options = {
         size: 1,
         sort: 'timestamp:desc'
      };

      return new Promise(function (resolve, reject) {
         self.search(query, index, options)
            .then(
               (response) => {
                  if (!_.isEmpty(response)) {
                     resolve(_.head(response)._source);
                  } else {
                     resolve([]);
                  }
               },
               (error) => {
                  reject(error);
               }
            );
      });
   }
}

const instance = new Elasticsearch();
Object.freeze(instance);
exports.instance = instance;
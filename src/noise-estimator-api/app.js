// Copyright (C) 2018 ATOS Spain
// 
// This file is part of Synchronicity.
// 
// Synchronicity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Synchronicity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Synchronicity.  If not, see <http://www.gnu.org/licenses/>.


var createError = require('http-errors');
var express = require('express');
var path = require('path');

var indexRouter = require('./routes/index');
const logger = require('./utils/log')(module);

var app = express();
var orion = require('./app/orion').instance;
var subscriptions = require('./app/orion-subscriptions').instance;
var notifications_handler = require('./routes/subscriptions');
var predictions_handler = require('./routes/predictions');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/notify', notifications_handler);
app.use('/predictions', predictions_handler);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

logger.info('Starting container...');

process.setMaxListeners(50);

// Init ORION
orion.start();

async function handle(signal) {  
  logger.info(`Caught interrupt signal - ${signal}`); 
  await subscriptions.close().catch (e => {
    logger.error ('Error closing the process ' + e);
  });
  process.exit();
}

// // CTRL + C Events
process.on('SIGINT', handle);
process.on('SIGTERM', handle);

// // Kill pid events (for example: nodemon restart)
process.on('SIGUSR1', handle);
process.on('SIGUSR2', handle);

// Uncaught exceptions
process.on('uncaughtException', handle);


module.exports = app;
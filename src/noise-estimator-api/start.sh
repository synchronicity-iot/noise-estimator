#! /bin/sh

# Helping script that waits till the Elasticsearch container is up -and-running
# Environment variable, by default, checks within the same host (Docker container: http://elasticsearch:9200)
# wait-on $ELASTIC_SEARCH_HOST  -d 2000 -i 5000 -t 120000 -v && npm start

echo "Starting application (wait-on to Elastic)... "
wait-on "${ELASTIC_SEARCH_HOST}"  -d 0 -i 2000 -t 120000 && 
exec npm start
wait 
var express = require('express');
var router = express.Router();

// var orion = require('../app/orion').instance;
var logger = require('../utils/log')(module);

/* GET users listing. */
router.get('/', function (req, res) {
   res.send('respond with a resource');
});

// Subscriptions handler
router.post('/', function (req, res) {

   logger.info('Prediction(s) received: ' + JSON.stringify(req.body, null, 2));
   //   orion.subs.managePrediction (req.body);
   return res.status(200).send();
});

module.exports = router;
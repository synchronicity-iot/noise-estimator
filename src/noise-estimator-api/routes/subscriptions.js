var express = require('express');
var router = express.Router();

var subscriptions = require('../app/orion-subscriptions').instance;
var logger = require('../utils/log')(module);

/* GET users listing. */
router.get('/', function(req, res) {
  res.send('respond with a resource');
});

// Subscriptions handler
router.post('/', function(req, res) {     
  logger.debug('Notification received - (' + req.body.subscriptionId + '): - ' + req.body.data.length + ' items');
  subscriptions.manageNotification (req.body.subscriptionId, req.body.data);  
  res.send('Post received');
});

module.exports = router;
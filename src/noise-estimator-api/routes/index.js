// Copyright (C) 2018 ATOS Spain
// 
// This file is part of Synchronicity.
// 
// Synchronicity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Synchronicity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Synchronicity.  If not, see <http://www.gnu.org/licenses/>.

'use strict';

const express = require('express');
const router = express.Router();
const cors = require('cors');
const _ = require('lodash');
const prettyHrtime = require('pretty-hrtime');

const config = require('../config/config');
const pagination = require('../utils/pagination');
const logger = require('../utils/log')(module);
const orion = require('../app/orion').instance;
const history = require('../app/historic-values').instance;
const estimator = require('../app/estimator').instance;

/**
 * Context element information (all)
 */
router.get('/' + config.api.root_path, cors(), function (req, res) {

  let option = 'elastic'; //Default search option (Elastic Search)  

  // Another option (force the system to use "Orion" to query) --> We assume that any other value leads to elastic
  // (either correct or not)
  if ((_.has(req.headers), 'source') && (_.toLower(req.headers.source) === 'orion')) {
    option = 'orion';
  }

  logger.debug('<REQ> GET context elements (all - ' + option + ')');
  const start = process.hrtime();

  orion.getEntitiesAtomicService(option).then(
    (response) => {
      let out = response.data;
      if (req.query.type) {
        out = _.filter(response.data, function (item) {
          return item.type === req.query.type;
        });
      }
      const end = process.hrtime(start);
      const latency = prettyHrtime(end, {
        precise: true,
        verbose: false
      });
      logger.perf('<RESP> GET context elements (all - ' + option + ')' +
        ' [' + latency + ']', {
          type: 'Get entities (all)',
          option: option,
          latency: latency,
          entities: response.data.length
        });

      // Recall to omit the new "marker" attribute created to address Grafana open issue
      return res.json(pagination.getPaginatedItems(_.map(out, o => {
          return _.omit(o, 'marker');
        }),
        req.query.page,
        req.query.items_per_page));
    }, (error) => {
      logger.error(error);
    }
  );
});

/**
 * Context element information (from its ID)
 */
router.get('/' + config.api.root_path + '/:id', cors(), function (req, res) {

  let option = 'elastic'; //Default search option (Elastic Search)
  if ((_.has(req.headers), 'source') && (_.toLower(req.headers.source) === 'orion')) {
    option = 'orion';
  }

  // Another option (force the system to use "Orion" to query) --> We assume that any other value leads to elastic
  // (either correct or not)
  logger.debug('<REQ> GET context info - ' + req.params.id + ' (' + option + ')');
  const start = process.hrtime();

  orion.getEntitiesAtomicService(option, req.params.id).then(
    (response) => {
      const end = process.hrtime(start);
      const latency = prettyHrtime(end, {
        precise: true,
        verbose: false
      });
      logger.perf('<RESP> GET context info - (' + option + ')' +
        ' [' + latency + ']', {
          type: 'Get context info (one)',
          option: option,
          latency: latency,
          entities: response.data.length
        });

      return res.json(_.omit(response.data, 'marker'));
    },
    (error) => {
      if (error.response.status === 404) {
        logger.error('<RESP> GET context info - Entity ' + req.params.id + ' not found');
        return res.status(error.response.status).send('Entity ' + req.params.id + ' not found');
      } else {
        return res.status(error.response.status).send();
      }
    }
  );
});

/**
 * Historical data (search via Elasticsearch)
 */
router.get('/' + config.api.root_path + '/:id/history', cors(),
  function (req, res) {

    orion.getEntitiesAtomicService('elastic', req.params.id).then(
      (response) => {
        // Two output formats (JSON or CSV)
        if (req.accepts('application/json') || req.accepts('*/*')) {

          // orion.historicalQuery(response.data, req.url.split('?')[1]).then(
          history.getHistory(response.data, req.url.split('?')[1]).then(
            (response) => {
              return (res.status(200).contentType('application/json').send(response));
            },
            (error) => {
              logger.error('Error getting historical data');
              return res.status(error.response.status).send();
            }
          );
        } else {
          logger.error('/' + config.api.root_path + '/:id/freeSpotEstimation/last1000 ' +
            'format not valid (' + req.headers.accept + ')');
          return res.status(415).send('Unsupported media type');
        }
      },
      (error) => {
        if (error.response.status === 404) {
          logger.error('Entity ' + req.params.id + ' not found');
          return res.status(error.response.status).send('Entity ' + req.params.id + ' not found');
        } else {
          return res.status(error.response.status).send();
        }
      });
  });


/**
 * Search ElasticSearch and yield a context element that includes the estimation attribute 
 *  - In afirmative case, we check the timestamp to see whether it is still valid and then sends back 
 *      the context element information
 *  - In case there is nothing on Orion, we forward the query to the AI-prediction engine
 */
router.get('/' + config.api.root_path + '/:id/' + config.api.estimator_path, cors(), function (req, res) {
  orion.getEntitiesAtomicService('elastic', req.params.id).then(
    (response) => {            
        estimator.estimation(response.data).then(
          (response) => {
            return res.json(_.omit(response, 'status_code'));
          },
          (error) => {            
            logger.error('Prediction not available (' + error.status_code + ') for - ' + req.params.id);
            return res.status(error.status_code).send('Prediction not available (' + error.status_code + ') for - ' + req.params.id);
          });      

    }, (error) => {
      logger.error('Error on getting the context element information, please check (' + error.response.status + ') - ' +
        req.params.id);
      return res.status(error.response.status).send('Error on getting the context element information, please check (' +
        error.response.status + ') - ' + req.params.id);
    }
  );
});

/**
 * Auxiliar option for those RZs that do not include Historical Data API 
 * Manual dump (Historical data)
 */
router.post('/newdata', function (req, res) {

  logger.debug('Manual historical data dump (' + req.body.length + ' observations)');

  if (req.headers['content-type'] === 'application/json') {
    history.manualDump(req.body).then(
      (response) => {       
        response; 
        return res.status(200).send();
      },
      (error) => {        
        return res.status(error.status_code).send();
      }
    );
  } else {
    logger.error('Wrong Content-Type field at HTTP Header');
    return res.status(415).send('Wrong Content-Type field at HTTP Header');
  }

});

module.exports = router;
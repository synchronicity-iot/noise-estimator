// Copyright (C) 2018 ATOS Spain
// 
// This file is part of Synchronicity.
// 
// Synchronicity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Synchronicity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Synchronicity.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Original source: https://gist.github.com/jstott/7b50d4f4790c357227bafd13b4ef32a4
 */

const _ = require('lodash');
const Math = require('mathjs');

/**
 * 
 * @param {Object} items 
 * @param {Number} page 
 * @param {Number} pageSize 
 * @returns Paginated object. NOTE: it is slightly different to the legacy one, as it includes
 * information about the pagination status
 */
function getPaginatedItems(items, page, pageSize) {

	const  pg = page || 1,
		pgSize = pageSize || items.length,
		offset = (pg - 1) * pgSize,
		pagedItems = items.length ? _.drop(items, offset).slice(0, pgSize) : [];

	return {
		summary: {
			current_page: pg,
			items_per_page: pgSize,
			items: items.length,
			pages: items.length ? Math.ceil(items.length / pgSize) : 0
		}, 
		resources: pagedItems
	};
}

exports.getPaginatedItems = getPaginatedItems;
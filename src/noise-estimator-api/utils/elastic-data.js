// Copyright (C) 2019 ATOS Spain
// 
// This file is part of Synchronicity.
// 
// Synchronicity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Synchronicity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Synchronicity.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Auxiliar variable with useful information for Elastic search
 */
const elasticdata = {      

   /**
    * Mapping of the context elements (devices)
    */
   entity_mapping: {
      context_entities: {
         properties: {
            id: {
               type: 'keyword',
               index: true
            }
         }
      }
   },

   /**
    * Data mapping (~observations from the sensors)
    */
   data_mapping: {
      context_data: {
         properties: {
            timestamp: {
               type: 'date',
               index: true
            },
            id: {
               type: 'keyword',
               index: true
            }
         }
      }
   }

};

module.exports = elasticdata;
// Copyright (C) 2018 ATOS Spain
// 
// This file is part of Synchronicity.
// 
// Synchronicity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Synchronicity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Synchronicity.  If not, see <http://www.gnu.org/licenses/>.

const config = require('../config/config.js');
const moment = require('moment');
const LEVEL = Symbol.for('level');

const {
    createLogger,
    format,
    transports,
    addColors
} = require('winston');


require('winston-daily-rotate-file');

const logLevels = {
    levels: {
        emerg: 0,
        alert: 1,
        crit: 2,
        error: 3,
        warning: 4,
        notice: 5,
        perf: 6,
        info: 7,
        debug: 8
    },
    colors: {
        emerg: 'red',
        alert: 'red',
        crit: 'red',
        error: 'red',
        warning: 'yellow',
        notice: 'blue',
        perf: 'magenta',
        info: 'cyan',
        debug: 'green'
    }
};
addColors(logLevels);

/**
 * Log only the messages the match `level`.
 */
function filterOnly(level) {
    return format(function (info) {
      if (info[LEVEL] === level) {
        return info;
      }
    })();
  }

var getLogger = function (module) {
    const logger = createLogger({
        level: 'info',  
        levels: logLevels.levels,      
        defaultMeta: {
            service: 'data-broker',
            source_file: module.filename.split('/').slice(-2).join('/'),
            '@timestamp': moment().toISOString()
        },
        transports: [
            //
            // - Write to all logs with level `info` and below to `combined.log` 
            // - Write all logs error (and below) to `error.log`.
            //
            new transports.Console({
                colorize: true,
                format: format.combine(
                    format.timestamp(),
                    format.colorize({
                        all: true
                    }),
                    format.align(),
                    format.printf((info) => {
                        const {
                            timestamp,
                            level,
                            source_file,
                            message,
                            // ...args
                        } = info;
                        return `${timestamp} ${level}: ${message} (${source_file})`;
                    })),
                timestamp: true,
                handleExceptions: true,
                humanReadableUnhandledException: true,
                prettyPrint: true,
                label: module.filename.split('/').slice(-2).join('/'),                
                level: config.logging.logLevel
            }),
            new transports.DailyRotateFile({
                filename: `../../logs/%DATE%-broker-perf.log`,      
                format: format.combine(
                    format.json(),
                    filterOnly('perf')
                  ),         
                datePattern: 'YYYY-MM-DD',
                prepend: true,
                label: module.filename.split('/').slice(-2).join('/'),
                json: true,
                maxsize: 5242880, // 5MB                              
                handleExceptions: false,
                humanReadableUnhandledException: false,                
                level: 'perf'
            }),
            new transports.DailyRotateFile({
                filename: `../../logs/%DATE%-broker-error.log`,      
                format: format.combine(
                    format.json(),  
                    filterOnly('error')                  
                  ),                           
                datePattern: 'YYYY-MM-DD',
                prepend: true,
                label: module.filename.split('/').slice(-2).join('/'),
                json: true,
                maxsize: 5242880, // 5MB                              
                handleExceptions: false,
                humanReadableUnhandledException: false,                
                level: 'error'
            }),
        ],
        exceptionHandlers: [
            new transports.DailyRotateFile({
                filename: `../../logs/%DATE%-broker-exceptions.log`,
                format: format.combine(
                    format.timestamp(),
                    format.json(),
                  ),         
                datePattern: 'YYYY-MM-DD',
                prepend: true,
                label: module.filename.split('/').slice(-2).join('/'),
                json: true,
                maxsize: 5242880, // 5MB                              
                handleExceptions: true,
                humanReadableUnhandledException: true,
            })
        ]
    });

    return logger;    
};

module.exports = getLogger;